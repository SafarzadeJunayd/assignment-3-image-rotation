#include "../include/bmp.h"
#define DEC_BASE 10
#define VALID_ARG_COUNT 4

int main(int argc, char *argv[]) {
    if (argc != VALID_ARG_COUNT){
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    char* src = argv[1];
    char* dest = argv[2];
    char *angle_end_ptr;
    int angle = (int)strtol(argv[3], &angle_end_ptr, DEC_BASE);
    if (*angle_end_ptr != '\0' || angle_end_ptr == argv[3] ||
       (angle != 0 && abs(angle) != 90 && abs(angle) != 180 && abs(angle) != 270)) {
        fprintf(stderr, "The requested angle value is invalid. Valid angle values are: 0, 90, -90, 180, -180, 270, -270\n");
        return 1;
    }

    struct image image;

    if (image_read(src, &image) != READ_SUCCESS) {
        fprintf(stderr, "Failed to read image from file\n");
        return 1;
    }
    if (image_rotate(&image, angle) != ROTATE_SUCCESS) {
        fprintf(stderr, "Failed to rotate image\n");
        return 1;
    }
    if (image_write(dest, &image) != WRITE_SUCCESS) {
        fprintf(stderr, "Failed to rotate image\n");
        return 1;
    }
    image_free(&image);
    return 0;
}
