#include "../include/bmp.h"
enum read_status from_bmp(FILE* file, struct image* image) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, file) != 1)
        return READ_HEADER_FAIL;
    if (header.bfType != BM_MARKER)
        return READ_INVALID_FORMAT;

    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;

    *image = image_create(width, height);
    if (!image->pixels)
        return READ_MEMORY_ALLOCATION_FAIL;

    size_t row_size = width * sizeof(struct pixel);
    int offset = (int)(row_size) % ROW_ALIGNMENT_BYTES;
    int padding = 0;
    if (offset > 0) padding = ROW_ALIGNMENT_BYTES - offset;

    fseek(file, (long)header.bOffBits, SEEK_SET);
    for (int y = 0; y < height; y++) {
        if(fread(&image->pixels[y * width], sizeof(struct pixel), width, file) != width) {
            return READ_PIXELS_FAIL;
        }
        fseek(file, padding, SEEK_CUR);
    }
    return READ_SUCCESS;
}

enum write_status to_bmp(FILE* file, const struct image* image) {
    struct bmp_header header = {
            .bfType = BM_MARKER,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = DEFAULT_HEADER_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = DEFAULT_PLANES,
            .biBitCount = DEFAULT_BIT_COUNT
    };

    size_t row_size = header.biWidth * sizeof(struct pixel);
    int offset = (int)(row_size) % ROW_ALIGNMENT_BYTES;
    int padding = 0;
    if (offset > 0) padding = ROW_ALIGNMENT_BYTES - offset;

    header.biSizeImage = (header.biWidth * sizeof(struct pixel) + padding) * header.biHeight;
    header.bfileSize = header.bOffBits + header.biSizeImage;

    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1)
        return WRITE_HEADER_FAIL;

    for (int y = 0; y < header.biHeight; y++) {
        if (fwrite(&(image->pixels[y * header.biWidth]), sizeof(struct pixel), header.biWidth, file) != header.biWidth) {
            return WRITE_PIXELS_FAIL;
        }
        int padding_byte = 0;
        for(int i = 0; i < padding; i++) {
            if (fwrite(&padding_byte, 1, 1, file) != 1) {
                return WRITE_PADDING_FAIL;
            }
        }
    }
    return WRITE_SUCCESS;
}

enum read_status image_read(const char* src, struct image* image) {
    FILE* file = fopen(src, "rb");
    if (!file) return READ_FILE_OPEN_FAIL;
    enum read_status read_status = from_bmp(file, image);
    if (fclose(file)) return READ_FILE_CLOSE_FAIL;
    return read_status;
}

enum write_status image_write(const char* dest, const struct image* image) {
    FILE* file = fopen(dest, "wb");
    if (!file) return WRITE_FILE_OPEN_FAIL;
    enum write_status write_status = to_bmp(file, image);
    if (fclose(file)) return WRITE_FILE_CLOSE_FAIL;
    return write_status;
}
