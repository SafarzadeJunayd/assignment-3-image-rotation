#include "../include/image.h"
struct image image_create(uint64_t width, uint64_t height) {
    return (struct image) {width, height, malloc(width * height * sizeof(struct pixel))};
}

void image_free(struct image* image) {
    free(image->pixels);
}
