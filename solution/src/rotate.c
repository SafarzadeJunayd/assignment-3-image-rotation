#include "../include/rotate.h"
enum rotation_status image_rotate(struct image *image, int angle) {
    uint64_t width = image->width;
    uint64_t height = image->height;

    struct image image_rotated;

    switch (angle) {
        case(0):
            return ROTATE_SUCCESS;
        case(180):case(-180):
            image_rotated = image_create(width, height);
            if (!image_rotated.pixels) return ROTATE_MEMORY_ALLOCATION_FAIL;
            for (uint32_t y = 0; y < height; y++)
                for (uint32_t x = 0; x < width; x++)
                    image_rotated.pixels[y * width + width - x - 1] = image->pixels[(height - y - 1) * width + x];
            break;
        case(90):case(-270):
            image_rotated = image_create(height, width);
            if (!image_rotated.pixels) return ROTATE_MEMORY_ALLOCATION_FAIL;
            for (uint32_t y = 0; y < height; y++)
                for (uint32_t x = 0; x < width; x++)
                    image_rotated.pixels[height * (width - x - 1) + y] = image->pixels[width * y + x];
            break;
        case(270):case(-90):
            image_rotated = image_create(height, width);
            if (!image_rotated.pixels) return ROTATE_MEMORY_ALLOCATION_FAIL;
            for (uint32_t y = 0; y < height; y++)
                for (uint32_t x = 0; x < width; x++)
                    image_rotated.pixels[height * x + height - y - 1] = image->pixels[width * y + x];
            break;
        default:
            return ROTATE_INVALID_ANGLE;
    }
    image_free(image);
    *image = image_rotated;
    return ROTATE_SUCCESS;
}
