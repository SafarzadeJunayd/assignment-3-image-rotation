#include "./rotate.h"
#include <stdio.h>
#include <stdlib.h>
#define BM_MARKER 0x4D42
#define ROW_ALIGNMENT_BYTES 4
#define DEFAULT_HEADER_SIZE 40
#define  DEFAULT_BIT_COUNT 24
#define DEFAULT_PLANES 1
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_SUCCESS ,
    READ_FILE_OPEN_FAIL,
    READ_FILE_CLOSE_FAIL,
    READ_HEADER_FAIL,
    READ_PIXELS_FAIL,
    READ_INVALID_FORMAT,
    READ_MEMORY_ALLOCATION_FAIL
};

enum read_status from_bmp(FILE* file, struct image* image);

enum write_status {
    WRITE_SUCCESS,
    WRITE_FILE_OPEN_FAIL,
    WRITE_FILE_CLOSE_FAIL,
    WRITE_HEADER_FAIL,
    WRITE_PIXELS_FAIL,
    WRITE_PADDING_FAIL
};

enum write_status to_bmp(FILE* file, const struct image* image);

enum read_status image_read(const char* src, struct image* image);

enum write_status image_write(const char* dest, const struct image* image);
