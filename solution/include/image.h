#ifndef IMAGE_H
#define IMAGE_H
#include <malloc.h>
#include <stdint.h>
struct __attribute__((packed)) pixel {
    uint8_t blue;
    uint8_t green;
    uint8_t red;
};
struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* pixels;
};

struct image image_create(uint64_t width, uint64_t height);

void image_free(struct image* image);
#endif
