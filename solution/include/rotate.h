#ifndef ROTATE_H
#define ROTATE_H
#include "./image.h"
#include <math.h>
#include <stdlib.h>

enum rotation_status {
    ROTATE_SUCCESS,
    ROTATE_INVALID_ANGLE,
    ROTATE_MEMORY_ALLOCATION_FAIL
};

enum rotation_status image_rotate(struct  image* image, int angle);
#endif
